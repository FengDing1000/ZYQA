
module.exports = function (err, req, res, next){
    if(err) {
        if (req.xhr) {
            res.apiError(err)
        }
        else {
            res.pageError(err)
        }
    }
    else {
        next()
    }
}