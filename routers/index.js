const express = require('express')
const db = require('../db/db')


const router = express.Router()

router.get(['/', '/k-:keyword([A-Za-z0-9]{3,20})'], (req, res) => {
    var filter = {}
    if (req.params.keyword) filter.keyword = req.params.keyword

    const q = db.Question.listByPager(filter)
    const k = db.Keyword.list()

    Promise.all([q, k])
        .then(([questions, keywords]) => {
            res.render('index', {
                user: req.cookies.user,
                keyword: req.params.keyword,
                keywords,
                questions
            })
        })
        .catch(err => {
            res.pageError(err)
        })
})


module.exports = router