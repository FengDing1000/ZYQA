const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Answer = {}


const answerSchema = Schema({
    text: {
        type: String,
        required: [true, '请填写回答内容！'],
        minlength: [1, '回答至少1个字符！'],
        maxlength: [200, '回答内容最多200个字符！']
    },
    question: {
        type: Schema.Types.ObjectId,
        ref: 'questions'
    },
    createUser: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    createTime: Date,
    ip: String
})


Answer.Model = mongoose.model('answers', answerSchema)


module.exports = Answer