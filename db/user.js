const mongoose = require('mongoose')
const Schema = mongoose.Schema
const User = {}

const userSchema = Schema({
    account: { 
        type: String, 
        unique: true,
        required: [true, '请填写账号！'],
        match: [/^[\u4e00-\u9fa5]{2,6}$/, '帐号是2~6个中文字符！'],
        minlength: [2, '账号至少2个字符！'],
        maxlength: [20, '账号最多20个字符！']
    },
    password: {
        type: String,
        required: [true, '请填写密码！'],
        minlength: [6, '密码至少6个字符！'],
        maxlength: [20, '密码最多20个字符！']
    },
    photo: String,
    createTime: Date,
    ip: String,
})


User.Model = mongoose.model('users', userSchema)


User.Model.on('index', err => {
    if (err) {
        console.error('集合 keywords 索引错误！', err)
    }
    else {
        console.log('集合 users 索引成功...')
    }
})


module.exports = User