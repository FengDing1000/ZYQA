const mongoose = require('mongoose')
const tools = require('./tools')


// ----设置Promise-------------------------------------------


mongoose.Promise = global.Promise


// ----数据模型------------------------------------------------


exports.User = require('./user')
exports.Keyword = require('./keyword')
exports.Question = require('./question')
exports.Answer = require('./answer')

exports.toData = tools.toData
exports.toDatas = tools.toDatas


// ----数据库-------------------------------------------------


exports.isDuplicateIndexError = function (err) {
    return err.name && err.name == 'MongoError' && err.code == 11000
}


// ----格式化数据-----------------------------------------------
exports.formatIP = function (ip) {
    if (ip.startsWith('::1')) {
        return '127.0.0.1'
    }
    if (ip.startsWith('::ffff:')) {
        return ip.substr(7)
    }
    return ip
}


// ----连接数据库-----------------------------------------------


mongoose.connect('mongodb://127.0.0.1/zyqa')
mongoose.connection.on('connected', () => console.log('数据库连接成功...'))


