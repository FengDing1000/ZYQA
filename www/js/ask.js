function addKeyword() {
    var keyword = prompt('请填写要添加的关键词：', '')
    if (keyword) {
        keyword = keyword.trim()
    }

    if (keyword) {
        $.ajax({
            url: '/api/keyword/add',
            data: { text: keyword },
            type: 'POST',
            dataType: 'json'
        }).done(function (res) {
            if (res.code == 'success') {
                $('<li>' + keyword + '</li>').appendTo('ul')
            }
            else {
                alert(res.message)
            }
        }).fail(function () {
            alert('网络连接失败，请稍后再试！')
        })
    }
}

$('ul').delegate('li', 'click', function () {
    $(this).addClass('selected')
        .siblings().removeClass('selected')
})

$('form').submit(function (ev) {
    ev.preventDefault()

    var keyword = $('li.selected').first().text()

    if (keyword) {
        var text = $('textarea').val().trim()
        if (text) {
            $.ajax({
                url: '/api/ask/add',
                data: { keyword, text },
                type: 'POST',
                dataType: 'json'
            }).done(function (res) {
                if (res.code == 'success') {
                    location.href = '/'
                }
                else {
                    alert(res.message)
                }
            }).fail(function (jqXHR) {
                alert('网络连接失败，请稍后再试！')
            })
        }
        else {
            alert('请填写问题！')
        }
    }
    else {
        alert('请选择一个关键词！')
    }
})