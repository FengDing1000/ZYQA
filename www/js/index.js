
$('li.selected,li.selected a').click(function (ev) {
    ev.preventDefault()
    location.href = '/'
})

function removeQuestion(id) {
    $.ajax({
        url: '/api/ask/remove/' + id,
        type: 'POST',
        dataType: 'json'
    })
        .done(function (res) {
            alert(res.message)
            if (res.code == 'success') {
                $('[qid=' + id + ']').remove()
            }
        })
        .fail(function () {
            alert('网络连接失败，请稍后再试！')
        })
}

function removeAnswer(id) {
    $.ajax({
        url: '/api/answer/remove/' + id,
        type: 'POST',
        dataType: 'json'
    })
        .done(function (res) {
            alert(res.message)
            if (res.code == 'success') {
                $('[aid=' + id + ']').remove()
            }
        })
        .fail(function () {
            alert('网络连接失败，请稍后再试！')
        })
}