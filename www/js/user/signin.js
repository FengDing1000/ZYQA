$('form').submit(function (ev) {
    ev.preventDefault()

    var data = $(this).serialize()

    $.ajax({
        url: '/api/user/signin',
        data: data,
        type: 'POST',
        dataType: 'JSON'
    })
    .done(function (res) {
        if (res.code == 'success') {
            location.href = '/'
        }
        else{
            alert(res.message)
        }
    })
    .fail(function(){
        alert('网络连接失败，请稍后再试！')
    })
})