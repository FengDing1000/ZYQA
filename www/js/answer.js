$('form').submit(function (ev) {
    ev.preventDefault()

    var text = $('textarea').val().trim()
    if (text) {
        $.ajax({
            url: '/api/answer/add/' + $('h3[qid]').attr('qid'),
            data: { text },
            type: 'POST',
            dataType: 'json'
        }).done(function (res) {
            if (res.code == 'success') {
                location.href = '/'
            }
            else {
                alert(res.message)
            }
        }).fail(function (jqXHR) {
            alert('网络连接失败，请稍后再试！')
        })
    }
    else {
        alert('请填写回答！')
    }
})