// 登录检查请求过滤器
exports.signin = function(req, res, next){
    if(req.cookies.user){
        next()
    }
    else{
        if(req.xhr){
            res.json({
                code:'not signin', 
                message: '登录超时，请重新登录！'
            })
        }
        else{
            res.redirect('/user/signin')
        }
    }
}