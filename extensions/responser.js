// 为res添加自定义的快捷响应方法
module.exports = function () {
    return function (req, res, next) {

        res.apiSuccess = function (message = '操作成功！') {
            res.json({ code: 'success', message })
        }

        res.apiMessage = function (message, code = 'error') {
            res.json({ code, message })
        }

        res.apiError = function (err) {
            console.error(err)
            if (err.name == 'AssertError') {
                res.json({ code: 'error', message: err.message })
            }
            else {
                res.json({ code: 'error', message: '服务端错误，请稍后再试！' })
            }

        }

        res.pageError = function (err) {
            console.error(err)
            res.render('error', err)
        }

        next()
    }
}